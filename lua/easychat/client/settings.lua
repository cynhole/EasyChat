-- general
local EC_ENABLE     = GetConVar("easychat_enable")
local EC_NO_MODULES = GetConVar("easychat_no_modules")

-- timestamps
local EC_TIMESTAMPS = GetConVar("easychat_timestamps")

-- teams and colors
local EC_TEAMS         = GetConVar("easychat_teams")
local EC_TEAMS_COLOR   = GetConVar("easychat_teams_colored")
local EC_PLAYER_COLOR  = GetConVar("easychat_players_colored")
local EC_PLAYER_PASTEL = GetConVar("easychat_pastel")

-- misc
local EC_LOCAL_MSG_DIST = GetConVar("easychat_local_msg_distance")
local EC_TICK_SOUND     = GetConVar("easychat_tick_sound")

-- chatbox
local EC_GLOBAL_ON_OPEN = GetConVar("easychat_global_on_open")
local EC_FONT           = GetConVar("easychat_font")
local EC_FONT_SIZE      = GetConVar("easychat_font_size")
local EC_HISTORY_FONT   = GetConVar("easychat_history_font")
local EC_TEAM_BTN       = GetConVar("easychat_team_btn")
local EC_TEAM_LUA       = GetConVar("easychat_team_lua")
local EC_NEW_TEXTENTRY  = GetConVar("easychat_textentryx")

-- chathud
local EC_HUD_FOLLOW     = GetConVar("easychat_hud_follow")
local EC_HUD_TTL        = GetConVar("easychat_hud_ttl")
local EC_HUD_SMOOTH     = GetConVar("easychat_hud_smooth")
local EC_HUD_YPOS       = GetConVar("easychat_hud_ypos")
local EC_CHAT_PRINT_COL = GetConVar("easychat_chatprint_color")

local default_chatprint_color = "151 211 255"
local function StrToCol(str)
    local col = Color(255,255,255)

    local r,g,b = str:match("(%d+) (%d+) (%d+)")

    col.r = tonumber(r) or 255
    col.g = tonumber(g) or 255
    col.b = tonumber(b) or 255

    return col
end

local function create_option_set(settings, category_name, options)
    for cvar, description in pairs(options) do
        settings:AddConvarSetting(category_name, "boolean", cvar, description)
    end

    local setting_reset_options = settings:AddSetting(category_name, "action", "Reset Options")
    setting_reset_options.DoClick = function()
        for cvar, _ in pairs(options) do
            local default_value = tobool(cvar:GetDefault())
            cvar:SetBool(default_value)
        end
    end
end

local function create_default_settings()
    local settings = EasyChat.Settings

    -- general settings
    do
        local category_name = "General"
        settings:AddCategory(category_name)

        create_option_set(settings, category_name, {
            [EC_TIMESTAMPS] = "Display timestamps",
            [EC_TEAMS] = "Display teams",
            [EC_TEAMS_COLOR] = "Color the team tags",
            [EC_PLAYER_COLOR] = "Color players in their team color",
            [EC_PLAYER_PASTEL] = "Pastellize player colors",
            [EC_TICK_SOUND] = "Tick sound on new messages",
        })

        settings:AddSpacer(category_name)

        settings:AddConvarSetting(category_name, "number", EC_LOCAL_MSG_DIST, "Local Message Distance", 1000, 100)

        local setting_reset_local_distance = settings:AddSetting(category_name, "action", "Reset Message Distance")
        setting_reset_local_distance.DoClick = function()
            local default_distance = tonumber(EC_LOCAL_MSG_DIST:GetDefault())
            EC_LOCAL_MSG_DIST:SetInt(default_distance)
        end

        settings:AddSpacer(category_name)

        local setting_disable_modules = settings:AddSetting(category_name, "action", EC_NO_MODULES:GetBool() and "Run Modules" or "Disallow Modules")
        setting_disable_modules.DoClick = function() EC_NO_MODULES:SetBool(not EC_NO_MODULES:GetBool()) end

        local setting_reload_ec = settings:AddSetting(category_name, "action", "Reload EasyChat")
        setting_reload_ec.DoClick = function() EasyChat.Reload() end

        local setting_disable_ec = settings:AddSetting(category_name, "action", "Disable EasyChat")
        setting_disable_ec.DoClick = function() EC_ENABLE:SetBool(false) end
    end

    -- chatbox settings
    do
        local category_name = "Chatbox"
        settings:AddCategory(category_name)

        create_option_set(settings, category_name, {
            [EC_GLOBAL_ON_OPEN] = "Open in the global tab",
            [EC_NEW_TEXTENTRY] = "Use TextEntryX (requires Chromium supported branch of the game)",
            [EC_TEAM_LUA] = "Open Lua tab on team chat",
        })

        settings:AddSpacer(category_name)

        settings:AddConvarSetting(category_name, "string", EC_FONT, "Font")
        settings:AddConvarSetting(category_name, "number", EC_FONT_SIZE, "Font Size", 128, 5)

        local setting_reset_font = settings:AddSetting(category_name, "action", "Reset Font")
        setting_reset_font.DoClick = function()
            local default_font, default_font_size = EC_FONT:GetDefault(), tonumber(EC_FONT_SIZE:GetDefault())
            EC_FONT:SetString(default_font)
            EC_FONT_SIZE:SetInt(default_font_size)
        end

        settings:AddConvarSetting(category_name, "font", EC_HISTORY_FONT, "History Font")
    end

    -- chathud settings
    do
        local category_name = "ChatHUD"
        settings:AddCategory(category_name)

        create_option_set(settings, category_name, {
            [EC_HUD_FOLLOW] = "Follow chatbox window",
            [EC_HUD_SMOOTH] = "Smooth message transitions"
        })

        settings:AddSpacer(category_name)

        settings:AddConvarSetting(category_name, "number", EC_HUD_TTL, "Message Duration", 60, 2)

        local setting_reset_duration = settings:AddSetting(category_name, "action", "Reset Duration")
        setting_reset_duration.DoClick = function()
            local default_duration = tonumber(EC_HUD_TTL:GetDefault())
            EC_HUD_TTL:SetInt(default_duration)
        end

        settings:AddConvarSetting(category_name, "float", EC_HUD_YPOS, "Vertical Position", 0, 1, 2)
    end
end

local function add_chathud_markup_settings()
    local settings = EasyChat.Settings
    local category_name = "ChatHUD"

    settings:AddSpacer(category_name)

    local tag_options = {}
    for part_name, _ in pairs(EasyChat.ChatHUD.Parts) do
        local cvar = GetConVar("easychat_tag_" .. part_name)
        if cvar then
            tag_options[cvar] = ("%s tags"):format(part_name)
        end
    end

    create_option_set(settings, category_name, tag_options)

    settings:AddSpacer(category_name)

    local setting_font_editor = settings:AddSetting(category_name, "action", "Font Editor")
    setting_font_editor.DoClick = function()
        local editor = vgui.Create("ECChatHUDFontEditor")
        editor:MakePopup()
        editor:Center()
    end

    local chat_print_color = settings:AddSetting(category_name, "color", "Chat Print Color")
    chat_print_color.Alpha:Remove()
    chat_print_color:SetColor(StrToCol(EC_CHAT_PRINT_COL:GetString()))
    chat_print_color.OnValueChanged = function(_, color)
        EC_CHAT_PRINT_COL:SetString(("%d %d %d"):format(color.r, color.g, color.b))
        EasyChat.TextColor = color
    end

    local chat_print_color_reset = settings:AddSetting(category_name, "action", "Reset Chat Print Color")
    chat_print_color_reset.DoClick = function()
        EC_CHAT_PRINT_COL:SetString(default_chatprint_color)
        EasyChat.TextColor = StrToCol(default_chatprint_color)
    end
end

local function add_legacy_settings()
    local options = {}
    for _, registered_cvar in pairs(EasyChat.GetRegisteredConvars()) do
        options[registered_cvar.Convar] = registered_cvar.Description
    end

    create_option_set(EasyChat.Settings, "Others", options)
end

local function add_team_mode_setting()
    local settings = EasyChat.Settings
    local category_name = "Chatbox"

    settings:AddConvarSetting(category_name, "combobox", EC_TEAM_BTN, "Team Button Mode", EasyChat.Modes)
end

hook.Add("ECPreLoadModules", "EasyChatDefaultSettings", create_default_settings)
hook.Add("ECPostLoadModules", "EasyChatDefaultSettings", function()
    add_chathud_markup_settings()
    add_team_mode_setting()
    add_legacy_settings()
end)