include("easychat/client/vgui/textentryx.lua")
include("easychat/client/vgui/emote_picker.lua")

local HAS_CHROMIUM = BRANCH ~= "dev" and BRANCH ~= "unknown"
local EC_NEW_TEXTENTRY = GetConVar("easychat_textentryx")

local MAIN_TAB = {}
function MAIN_TAB:Init()
    local lastFont = GetConVar("easychat_history_font"):GetString()

    self.RichText  = self:Add("RichText")
    self.InpBox    = self:Add("EditablePanel")
    self.BtnSwitch = self.InpBox:Add("DButton")
    if HAS_CHROMIUM and EC_NEW_TEXTENTRY:GetBool() then
        self.TextEntry = self.InpBox:Add("TextEntryX")
        self.TextEntry:SetBackgroundColor(Color(0, 0, 0, 0))
        self.TextEntry:SetBorderColor(Color(0, 0, 0, 0))
        self.TextEntry:SetTextColor(self.TextEntry:GetSkin().Colours.Label.Dark)
        self.TextEntry.Paint = function(s, w, h)
            s:GetSkin().tex.TextBox(0, 0, w, h)
        end
    else
        self.TextEntry = self.InpBox:Add("DTextEntry")
        self.TextEntry.OnTab = function() end
        self.TextEntry:SetSkin("Default")
        self.TextEntry:SetHistoryEnabled(true)
        self.TextEntry.HistoryPos = 0
        self.TextEntry:SetUpdateOnType(true)

        local last_key = KEY_ENTER
        self.TextEntry.OnKeyCodeTyped = function(s, key_code)
            EasyChat.SetupHistory(s, key_code)
            EasyChat.UseRegisteredShortcuts(s, last_key, code)

            if key_code == KEY_TAB then
                s:OnTab()
            elseif key_code == KEY_ENTER or key_code == KEY_PAD_ENTER then
                s:OnEnter()
            end

            last_key = key_code
        end
    end

    self.RichText:Dock(FILL)
    self.RichText:SetVerticalScrollbarEnabled(true)
    self.RichText.Think = function(s)
        if lastFont ~= GetConVar("easychat_history_font"):GetString() then
            s:InvalidateLayout()
            lastFont = GetConVar("easychat_history_font"):GetString()
        end
    end
    self.RichText.PerformLayout = function(s)
        s:SetFontInternal(GetConVar("easychat_history_font"):GetString() or "DermaDefault")
        s:SetFGColor(Color(255,255,255,255))
    end
    self.RichText.Paint = function(s,w,h)
        draw.RoundedBox(0,0,0,w,h,Color(36,36,36,240))
    end

    self.InpBox:Dock(BOTTOM)
    self.InpBox:SetTall(20)

    self.BtnSwitch:SetText("Say")
    self.BtnSwitch:Dock(LEFT)
    self.BtnSwitch:SetWide(54)
    self.BtnSwitch:SetZPos(10)
    self.BtnSwitch.Think = function(s)
        if EasyChat.Mode == 0 then
            s:SetText("Say")
        else
            s:SetText(EasyChat.Modes[EasyChat.Mode].Name)
        end
    end
    self.BtnSwitch.DoClick = function()
        local next_mode = EasyChat.Mode + 1
        EasyChat.Mode = next_mode > EasyChat.ModeCount and 0 or next_mode
    end

    local ModeIcons = {
        "icon16/flag_blue.png",
        "icon16/group.png",
        "icon16/application_xp_terminal.png",
        "icon16/sound.png"
    }

    self.BtnSwitch.DoRightClick = function()
        local m = DermaMenu()
        local say = m:AddOption("Say", function() EasyChat.Mode = 0 end)
        say:SetIcon("icon16/comment.png")
        for k,v in next,EasyChat.Modes do
            local o = m:AddOption(v.Name, function() EasyChat.Mode = k end)
            if ModeIcons[k] then
                o:SetIcon(ModeIcons[k])
            end
        end

        m:Open()
    end

    self.TextEntry:Dock(FILL)
    self.TextEntry:SetZPos(10)

    self.Picker = vgui.Create("ECEmotePicker")
    self.Picker:SetVisible(false)
    self.Picker.OnEmoteClicked = function(_, emote_name)
        local text = ("%s :%s:"):format(self.TextEntry:GetText():Trim(), emote_name)
        self.TextEntry:SetText(text)

        if input.IsKeyDown(KEY_LSHIFT) or input.IsKeyDown(KEY_RSHIFT) then return end
        self.Picker:SetVisible(false)
        self.TextEntry:RequestFocus()
    end

    local function on_key_code_typed(_, key_code)
        if key_code == KEY_ENTER or key_code == KEY_PAD_ENTER then
            self.TextEntry:OnEnter()
        end
    end

    self.Picker.Search.OnKeyCodeTyped = on_key_code_typed
    self.Picker.OnKeyCodePressed = on_key_code_typed

    local function on_picker_mouse_pressed()
        if not IsValid(self) then return end
        if not IsValid(self.Picker) then return end

        if not self.Picker:MouseInBounds() then
            self.Picker:SetVisible(false)
        end
    end

    hook.Add("GUIMousePressed", self.Picker, on_picker_mouse_pressed)
    hook.Add("VGUIMousePressed", self.Picker, on_picker_mouse_pressed)
    hook.Add("ECClosed", self.Picker, function()
        if not IsValid(self) then return end
        if not IsValid(self.Picker) then return end

        self.Picker:SetVisible(false)
    end)

    self.Picker.OnRemove = function(self)
        hook.Remove("GUIMousePressed", self)
        hook.Remove("VGUIMousePressed", self)
        hook.Remove("ECClosed", self)
    end

    self.BtnPicker = self.InpBox:Add("DButton")
    self.BtnPicker:Dock(RIGHT)
    self.BtnPicker:SetText("")
    self.BtnPicker:SetIcon("icon16/emoticon_smile.png")
    self.BtnPicker:SetWide(24)
    self.BtnPicker.DoClick = function()
        local btn_x, btn_y = self.BtnPicker:LocalToScreen(0, 0)
        self.Picker:SetPos(btn_x - self.Picker:GetWide() + self.BtnPicker:GetWide(), btn_y - self.Picker:GetTall())
        self.Picker:SetVisible(true)

        self.Picker:MakePopup()
        self.Picker.Search:SetText("")
        self.Picker.Search:RequestFocus()
        self.Picker:Populate()
    end
    self.BtnPicker.Paint = function() end
end

function MAIN_TAB:Paint() end

vgui.Register("ECChatTab",MAIN_TAB,"DPanel")
