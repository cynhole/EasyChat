local black_color = Color(0, 0, 0)

local SETTINGS = {}

function SETTINGS:Init()
    self.CategoryList = {}

    self.Categories = self:Add("DColumnSheet")
    self.Categories:Dock(FILL)
    self.Categories.Navigation:DockMargin(0, 0, 0, 0)
end

function SETTINGS:CreateNumberSetting(panel, name, max, min)
    local number_wang = panel:Add("DNumberWang")
    number_wang:SetMax(max)
    number_wang:SetMin(min)
    number_wang:Dock(TOP)
    number_wang:DockMargin(10, 10, 10, 10)

    number_wang.OldPaint = number_wang.Paint
    number_wang.Paint = function(s, w, h)
        s:OldPaint(w,h)

        surface.DisableClipping(true)
            surface.SetTextPos(0, -15)
            surface.SetTextColor(self:GetSkin().text_normal)
            surface.SetFont("DermaDefault")
            surface.DrawText(name)
        surface.DisableClipping(false)
    end

    return number_wang
end

function SETTINGS:CreateStringSetting(panel, name)
    local text_entry = panel:Add("DTextEntry")
    text_entry:Dock(TOP)
    text_entry:DockMargin(10, 10, 10, 10)

    text_entry.OldPaint = text_entry.Paint
    text_entry.Paint = function(s, w, h)
        s:OldPaint(w,h)

        surface.DisableClipping(true)
            surface.SetTextPos(0, -15)
            surface.SetTextColor(self:GetSkin().text_normal)
            surface.SetFont("DermaDefault")
            surface.DrawText(name)
        surface.DisableClipping(false)
    end

    return text_entry
end

function SETTINGS:CreateBooleanSetting(panel, description)
    local checkbox_label = panel:Add("DCheckBoxLabel")
    checkbox_label:SetText(description)
    checkbox_label:Dock(TOP)
    checkbox_label:DockMargin(10, 0, 10, 10)

    return checkbox_label
end

function SETTINGS:CreateActionSetting(panel, name)
    local btn = panel:Add("DButton")
    btn:Dock(TOP)
    btn:DockMargin(10, 0, 10, 10)
    btn:SetTall(25)
    btn:SetText(name)

    return btn
end

local COLOR_SETTING = {
    Init = function(self)
        self.Color = Color(0, 0, 0, 0)
        self:SetTall(30)

        self.Title = self:Add("DLabel")
        self.Title:SetTall(10)
        self.Title:SetText("Unknown")
        self.Title:Dock(TOP)
        self.Title:DockMargin(0, 0, 0, 5)

        self.Red = self:CreateWang()
        self.Red:SetTextColor(Color(255, 0, 0))
        self.Red.OnValueChanged = function(_, val)
            self.Color.r = val
            self:OnValueChanged(self.Color)
        end

        self.Green = self:CreateWang()
        self.Green:SetTextColor(Color(0, 255, 0))
        self.Green.OnValueChanged = function(_, val)
            self.Color.g = val
            self:OnValueChanged(self.Color)
        end

        self.Blue = self:CreateWang()
        self.Blue:SetTextColor(Color(0, 0, 255))
        self.Blue.OnValueChanged = function(_, val)
            self.Color.b = val
            self:OnValueChanged(self.Color)
        end

        self.Alpha = self:CreateWang()
        self.Alpha:SetTextColor(black_color)
        self.Alpha.OnValueChanged = function(_, val)
            self.Color.a = val
            self:OnValueChanged(self.Color)
        end

        self.Preview = self:Add("DPanel")
        self.Preview:Dock(LEFT)
        self.Preview:SetSize(30, 30)
        self.Preview:DockMargin(0, 0, 10, 0)
        self.Preview.Paint = function(_, w, h)
            surface.SetDrawColor(self.Color:Unpack())
            surface.DrawRect(0, 0, w, h)

            surface.SetDrawColor(black_color)
            surface.DrawOutlinedRect(0, 0, w, h)
        end
    end,
    CreateWang = function(self)
        local wang = self:Add("DNumberWang")
        wang:SetMax(255)
        wang:SetMin(0)
        wang:SetValue(0)
        wang:Dock(LEFT)
        wang:DockMargin(0, 0, 10, 0)
        wang:SetSize(50, 30)

        return wang
    end,
    GetColor = function(self) return self.Color end,
    SetColor = function(self, color)
        local new_col = Color(color.r, color.g, color.b, color.a)
        self.Color = new_col
        self.Red:SetValue(new_col.r)
        self.Green:SetValue(new_col.g)
        self.Blue:SetValue(new_col.b)
        self.Alpha:SetValue(new_col.a)
    end,
    SetTitle = function(self, title)
        self.Title:SetText(title)
    end,
    OnValueChanged = function(self, color)
    end,
}

vgui.Register("ECColorSetting", COLOR_SETTING, "DPanel")

function SETTINGS:CreateColorSetting(panel, name)
    local color_setting = panel:Add("ECColorSetting")
    color_setting:Dock(TOP)
    color_setting:DockMargin(10, 0, 10, 10)
    color_setting:SetTitle(name)
    color_setting:SetColor(color_white)

    local function entry_paint(s, w, h)
        s:GetSkin().tex.TextBox(0, 0, w, h)
        s:DrawTextEntryText(s.m_colText, s:GetHighlightColor(), s:GetSkin().Colours.Label.Dark)
    end

    color_setting.Paint = function() end
    color_setting.Red.Paint = entry_paint
    color_setting.Green.Paint = entry_paint
    color_setting.Blue.Paint = entry_paint
    color_setting.Alpha.Paint = entry_paint

    return color_setting
end

function SETTINGS:AddCategory(category_name)
    category_name = category_name or "???"
    if self.CategoryList[category_name] then return end

    local panel = vgui.Create("DScrollPanel")
    panel:DockMargin(0, 10, 0, 10)
    panel:Dock(FILL)

    local new_category = self.Categories:AddSheet(category_name, panel)
    new_category.Button:DockPadding(0, 20, 0, 20)

    new_category.Button:DockMargin(0, 0, 0, 0)
    self.CategoryList[category_name] = panel
end

function SETTINGS:GetCategory(category_name)
    category_name = category_name or "???"

    -- create the category if it doesnt exist
    if not self.CategoryList[category_name] then
        self:AddCategory(category_name)
    end

    return self.CategoryList[category_name]
end

function SETTINGS:AddChangeCallback(cvar, on_change)
    local cvar_name = cvar:GetName()
    local callback_name = ("EasyChatSetting_%s"):format(cvar_name)
    cvars.RemoveChangeCallback(cvar_name, callback_name)
    cvars.AddChangeCallback(cvar_name, on_change, callback_name)
end

local history_fonts = {
    Trebuchet24 = true,
    ChatFont = true,
    TargetID = true,
    TargetIDSmall = true,
    BudgetLabel = true,
    Default = true,
    DefaultFixed = true,
    DermaDefault = true,
    closecaption_normal = true,
    hudselectiontext = true,
    EasyChatHistoryFont = true,
}

local convar_type_callbacks = {
    ["number"] = function(self, panel, cvar, name, max, min)
        local number_wang = self:CreateNumberSetting(panel, name, max, min)
        number_wang:SetValue(cvar:GetInt())
        number_wang.OnValueChanged = function(_, new_value)
            cvar:SetInt(new_value)
        end

        self:AddChangeCallback(cvar, function()
            if not IsValid(number_wang) then return end
            number_wang:SetValue(cvar:GetInt())
        end)

        return number_wang
    end,
    ["string"] = function(self, panel, cvar, name)
        local text_entry = self:CreateStringSetting(panel, name)
        text_entry:SetText(cvar:GetString())
        text_entry.OnEnter = function(self)
            cvar:SetString(self:GetText():Trim())
        end

        self:AddChangeCallback(cvar, function()
            if not IsValid(text_entry) then return end
            text_entry:SetText(cvar:GetString())
        end)

        return text_entry
    end,
    ["boolean"] = function(self, panel, cvar, description)
        local checkbox_label = self:CreateBooleanSetting(panel, description)
        checkbox_label:SetChecked(cvar:GetBool())
        checkbox_label.OnChange = function(_, new_value)
            cvar:SetBool(new_value)
        end

        self:AddChangeCallback(cvar, function()
            if not IsValid(checkbox_label) then return end
            checkbox_label:SetChecked(cvar:GetBool())
        end)

        return checkbox_label
    end,
    ["font"] = function(self, panel, cvar, name)
        local font_selector = vgui.Create("DCollapsibleCategory", panel)
        font_selector:Dock(TOP)
        font_selector:DockMargin(10, 10, 10, 10)
        font_selector:SetLabel(name)
        font_selector:SetExpanded(true)

        for font_name, _ in next, history_fonts do
            local font = font_selector:Add(font_name)
            font:SetFont(font_name)
            if cvar:GetString() == font_name then
                font:SetSelected(true)
            end
            font.DoClick = function(s)
                cvar:SetString(s:GetText())
            end
        end

        font_selector:SizeToContents()

        self:AddChangeCallback(cvar, function()
            if not IsValid(font_selector) then return end
            for _, pnl in pairs(font_selector:GetChildren()) do
                if pnl:GetText() == cvar:GetString() then
                    pnl:SetSelected(true)
                end
            end
        end)

        return font_selector
    end,
    ["combobox"] = function(self, panel, cvar, name, options)
        local cb_container = vgui.Create("EditablePanel", panel)
        cb_container:Dock(TOP)
        cb_container:SetTall(24)
        cb_container:DockMargin(10, 10, 10, 10)

        local cb_label = cb_container:Add("DLabel")
        cb_label:Dock(LEFT)
        cb_label:DockMargin(4, 4, 36, 4)
        cb_label:SetText(name)
        cb_label:SizeToContents()

        local combobox = cb_container:Add("DComboBox")
        combobox:Dock(FILL)
        for id, data in pairs(options) do
            combobox:AddChoice(data.Name, id, (id - 1) == cvar:GetInt())
        end
        combobox.OnSelect = function(s, i, n, value)
            cvar:SetInt(value - 1)
        end

        return cb_container
    end,
    ["float"] = function(self, panel, cvar, name, min, max, precision)
        local slider = vgui.Create("DNumSlider", panel)
        slider:Dock(TOP)
        slider:DockMargin(10, 10, 10, 10)
        slider:SetConVar(cvar:GetName())
        slider:SetText(name)
        slider:SetMin(min)
        slider:SetMax(max)
        slider:SetDecimals(precision or 0)
        slider:SizeToContents()

        self:AddChangeCallback(cvar, function()
            if not IsValid(slider) then return end
            slider:SetChecked(cvar:GetFloat())
        end)

        return slider
    end,
}

function SETTINGS:AddConvarSetting(category_name, type, cvar, ...)
    if not convar_type_callbacks[type] then return end
    if not cvar then return end

    local category_panel = self:GetCategory(category_name)
    return convar_type_callbacks[type](self, category_panel, cvar, ...)
end

local type_callbacks = {
    ["number"] = SETTINGS.CreateNumberSetting,
    ["string"] = SETTINGS.CreateStringSetting,
    ["boolean"] = SETTINGS.CreateBooleanSetting,
    ["action"] = SETTINGS.CreateActionSetting,
    ["color"] = SETTINGS.CreateColorSetting,
}

function SETTINGS:AddSetting(category_name, type, ...)
    if not type_callbacks[type] then return end
    local category_panel = self:GetCategory(category_name)
    return type_callbacks[type](self, category_panel, ...)
end

function SETTINGS:AddSpacer(category_name)
    local category_panel = self:GetCategory(category_name)
    local spacer = category_panel:Add("EditablePanel")
    spacer:SetTall(2)
    spacer:Dock(TOP)
    spacer:DockMargin(0, 0, 0, 10)
end

vgui.Register("ECSettingsTab", SETTINGS, "DPanel")