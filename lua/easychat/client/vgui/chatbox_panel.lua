local CHATBOX = {}

local white_color = Color(255, 255, 255)
local black_color = Color(0, 0, 0)
local gray_bg_color = Color(66, 66, 66, 100)
local gray_frame_color = Color(66, 66, 66, 150)
local close_color = Color(255, 64, 64)
local close_hover_color = Color(255, 0, 0)
local close_pressed_color = Color(128, 32, 32)
local maxim_color = Color(175, 175, 175)
local maxim_hover_color = Color(127, 127, 127)
local maxim_pressed_color = Color(32, 32, 32)

function CHATBOX:Init()
    self:ShowCloseButton(true)
    self:SetDraggable(true)
    self:SetSizable(true)
    self:SetDeleteOnClose(false)
    self:SetTitle("")
    self:SetScreenLock(true)

    self.btnMaxim:SetEnabled(true)
    self.btnMinim:Hide()

    self.Tabs     = self:Add("DPropertySheet")
    self.Scroller = self.Tabs.tabScroller
    self.OldTab   = NULL

    self.Tabs.Paint = function() end

    self.btnClose:SetSize(42, 16)
    self.btnClose:SetZPos(10)
    self.btnClose.Paint = function(s, w, h)
        local bg = (s.Depressed and close_pressed_color) or (s:IsHovered() and close_hover_color) or close_color

        draw.RoundedBoxEx(4, 0, 0, w, h, bg, false, false, true, true)
        draw.SimpleTextOutlined("r", "Marlett", w / 2, h / 2, white_color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, black_color)
    end
    self.btnClose.Think = function(s)
        local x, y, w, h = self:GetBounds()
        s:SetPos(w - s:GetWide() - 8, 0)
    end

    self.btnMaxim:SetSize(42, 16)
    self.btnMaxim:SetZPos(10)
    self.btnMaxim.IsFullScreen = false
    self.btnMaxim.Paint = function(s,w,h)
        local bg = (s.Depressed and maxim_pressed_color) or (s:IsHovered() and maxim_hover_color) or maxim_color

        draw.RoundedBoxEx(4, 0, 0, w, h, bg, false, false, true, true)
        draw.SimpleTextOutlined(s.IsFullScreen and "2" or "1", "Marlett", w / 2, h / 2, white_color, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, black_color)
    end
    self.btnMaxim.DoClick = function(s)
        if not self.IsFullScreen then
            local a,b,c,d = self:GetBounds()
            self.Before = {
                x = a,
                y = b,
                w = c,
                h = d,
            }
            self:SetSize(ScrW(), ScrH())
            self:SetPos(0, 0)
            self.IsFullScreen = true
        else
            self:SetPos(self.Before.x, self.Before.y)
            self:SetSize(self.Before.w, self.Before.h)
            self.IsFullScreen = false
        end
    end
    self.btnMaxim.Think = function(s)
        local x, y, w, h = self:GetBounds()
        s:SetPos(w - s:GetWide() - 52, 0)
    end

    self.Tabs:SetPos(6, 6)

    self.Tabs.old_performlayout = self.Tabs.PerformLayout
    self.Tabs.PerformLayout = function(s)
        s.old_performlayout(s)
        self.Scroller:SetTall(22)
    end

    self.Tabs.Think = function(s)
        local x, y, w, h = self:GetBounds()
        s:SetSize(w - 13, h - 11)
        local current = s:GetActiveTab()
        if IsValid(self.OldTab) and current ~= self.OldTab then
            hook.Run("ECTabChanged", self.OldTab.Name, current.Name)
            self.OldTab = current
        end
    end

    self.Scroller:SetParent(self.Tabs)
    self.Scroller:Dock(TOP)
    self.Scroller:SetSize(0,20)
    self.Scroller.m_iOverlap = -2
end

function CHATBOX:Paint(w, h)
    surface.SetDrawColor(gray_bg_color)
    surface.DrawRect(0, 0, w, h)
    surface.SetDrawColor(gray_frame_color)
    surface.DrawOutlinedRect(0, 0, w, h)
end

function CHATBOX:PerformLayout() end

vgui.Register("ECChatBox",CHATBOX,"DFrame")
