local file_path = "easychat/chud_font_settings.txt"
local default_font_data = {
    font = system.IsWindows() and "Verdana" or "Roboto",
    extended = true,
    size = 17,
    weight = 600,
    shadow = true,
    read_speed = 100,
}

local props = {
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    rotary = false,
    shadow = false,
    additive = false,
    outline = false,
}

for prop_name, prop_default in pairs(props) do
    if default_font_data[prop_name] == nil then
        default_font_data[prop_name] = prop_default
    end
end

local EDITOR = {
    Init = function(self)
        self:LoadFontData()

        self:SetSize(600, 410)
        self:Center()
        self:SetTitle("EasyChat ChatHUD Font Editor")

        self.BaseFont = self:Add("DTextEntry")
        self.FontSize = self:Add("DNumSlider")
        self.FontWeight = self:Add("DNumSlider")
        self.FontScanlines = self:Add("DNumSlider")

        self.BaseFont:Dock(TOP)
        self.BaseFont:SetTall(20)
        self.BaseFont:DockMargin(4, 4, 4, 4)
        self.BaseFont:SetValue(self.FontData.font)

        self.FontSize:Dock(TOP)
        self.FontSize:SetTall(20)
        self.FontSize:DockMargin(4, 4, 4, 4)
        self.FontSize:SetDecimals(0)
        self.FontSize:SetText("Size")
        self.FontSize:SetMax(254)
        self.FontSize:SetMin(5)
        self.FontSize:SetValue(self.FontData.size)

        self.FontWeight:Dock(TOP)
        self.FontWeight:SetTall(20)
        self.FontWeight:DockMargin(4, 4, 4, 4)
        self.FontWeight:SetDecimals(0)
        self.FontWeight:SetText("Weight")
        self.FontWeight:SetMax(1000)
        self.FontWeight:SetMin(200)
        self.FontWeight:SetValue(self.FontData.weight)

        self.FontScanlines:Dock(TOP)
        self.FontScanlines:SetTall(20)
        self.FontScanlines:DockMargin(4, 4, 4, 4)
        self.FontScanlines:SetDecimals(0)
        self.FontScanlines:SetText("Scanlines")
        self.FontScanlines:SetMin(0)
        self.FontScanlines:SetMax(10)
        self.FontScanlines:SetValue(self.FontData.scanlines)

        for prop_name, prop_default in pairs(props) do
            local checkbox = self:Add("DCheckBoxLabel")
            if self.FontData[prop_name] ~= nil then
                checkbox:SetChecked(self.FontData[prop_name])
            else
                checkbox:SetChecked(prop_default)
            end

            checkbox:Dock(TOP)
            checkbox:SetTall(20)
            checkbox:DockMargin(4, 4, 4, 4)
            checkbox:SetText(prop_name:gsub("^.", string.upper))

            self[prop_name] = checkbox
        end

        self.ApplyFont = self:Add("DButton")
        self.ResetFont = self:Add("DButton")

        self.ApplyFont:Dock(TOP)
        self.ApplyFont:SetTall(22)
        self.ApplyFont:DockMargin(4, 4, 4, 1)
        self.ApplyFont:SetText("Apply Font")
        self.ApplyFont.DoClick = function()
            self:SaveFontData()
        end

        self.ResetFont:Dock(TOP)
        self.ResetFont:SetTall(22)
        self.ResetFont:DockMargin(4, 1, 4, 4)
        self.ResetFont:SetText("Reset Font")
        self.ResetFont.DoClick = function()
            self:ResetFontData()
        end
    end,
    LoadFontData = function(self)
        if file.Exists(file_path, "DATA") then
            local json = file.Read(file_path, "DATA")
            self.FontData = util.JSONToTable(json)
        else
            self.FontData = default_font_data
        end
    end,
    SaveFontData = function(self)
        local data = {
            font = self.BaseFont:GetValue(),
            extended = true,
            blursize = 0,
            size = self.FontSize:GetValue(),
            weight = self.FontWeight:GetValue(),
            scanlines = self.FontScanlines:GetValue(),
        }

        for prop_name, _ in pairs(props) do
            local checkbox = self[prop_name]
            data[prop_name] = checkbox:GetChecked()
        end

        local shadow_data = table.Copy(data)
        shadow_data.blursize = 1

        local chathud = EasyChat.ChatHUD
        surface.CreateFont(chathud.DefaultFont, data)
        surface.CreateFont(chathud.DefaultShadowFont, shadow_data)
        chathud:InvalidateLayout()

        local json = util.TableToJSON(data, true)
        file.Write(file_path, json)
    end,
    ResetFontData = function(self)
        local shadow_data = table.Copy(default_font_data)
        shadow_data.blursize = 1

        local chathud = EasyChat.ChatHUD
        surface.CreateFont(chathud.DefaultFont, default_font_data)
        surface.CreateFont(chathud.DefaultShadowFont, shadow_data)
        chathud:InvalidateLayout()

        if file.Exists(file_path, "DATA") then
            file.Delete(file_path)
        end
    end,
    Paint = function(self, w, h)
        surface.SetDrawColor(66, 66, 66, 100)
        surface.DrawRect(0, 0, w, h)
        surface.SetDrawColor(66, 66, 66, 150)
        surface.DrawOutlinedRect(0, 0, w, h)
    end,
}

vgui.Register("ECChatHUDFontEditor", EDITOR, "DFrame")

hook.Add("ECInitialized", "EasyChatChatHUDFontEditor", function()
    if not file.Exists(file_path, "DATA") then return end

    local json = file.Read(file_path, "DATA")
    local data = util.JSONToTable(json)
    local shadow_data = table.Copy(data)
    shadow_data.blursize = 1

    local chathud = EasyChat.ChatHUD
    surface.CreateFont(chathud.DefaultFont, data)
    surface.CreateFont(chathud.DefaultShadowFont, shadow_data)
    chathud:InvalidateLayout()
end)