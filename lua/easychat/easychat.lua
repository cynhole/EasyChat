local EasyChat = _G.EasyChat or {}
_G.EasyChat = EasyChat

local print  = _G._print or _G.print --epoe compat

local NET_BROADCAST_MSG = "EASY_CHAT_BROADCAST_MSG"
local NET_SEND_MSG      = "EASY_CHAT_RECEIVE_MSG"
local NET_SET_TYPING    = "EASY_CHAT_START_CHAT"

local PLY = FindMetaTable("Player")
local TAG = "EasyChat"

local color_print_head = Color(244, 167, 66)
local color_print_good = Color(0, 160, 220)
local color_print_bad = Color(255, 127, 127)
function EasyChat.Print(is_err, ...)
    local args = { ... }
    local body_color

    if isstring(is_err) then
        table.insert(args, 1, is_err)
        body_color = color_print_good
    else
        body_color = is_err and color_print_bad or color_print_good
    end

    for k, v in ipairs(args) do args[k] = tostring(v) end
    MsgC(color_print_head, "[EasyChat] ⮞ ", body_color, table.concat(args), "\n")
end

local load_modules, get_modules = include("easychat/autoloader.lua")
EasyChat.GetModules = get_modules -- maybe useful for modules?

function PLY:ECIsEnabled()
    return self:GetInfoNum("easychat_enable", 0) == 1
end

PLY.old_IsTyping = PLY.old_IsTyping or PLY.IsTyping
function PLY:IsTyping()
    if self:ECIsEnabled() then
        return self:GetNWBool("ec_is_typing", false)
    else
        return self:old_IsTyping()
    end
end

if SERVER then
    util.AddNetworkString(NET_SEND_MSG)
    util.AddNetworkString(NET_BROADCAST_MSG)
    util.AddNetworkString(NET_SET_TYPING)

    net.Receive(NET_SEND_MSG, function(len, ply)
        if not IsValid(ply) then return end

        local str = net.ReadString()
        local is_team = net.ReadBool()
        local is_local = net.ReadBool()
        local msg = gamemode.Call("PlayerSay", ply, str, is_team, is_local)

        if not isstring(msg) or msg:Trim() == "" then return end

        local filter = {}
        local broken_count = 1
        local function add_to_filter(pl)
            local id = pl:AccountID()
            if not id then
                filter[broken_count] = pl
                broken_count = broken_count + 1
            else
                filter[id] = pl
            end
        end

        add_to_filter(ply)
        for _, listener in ipairs(player.GetAll()) do
            if listener ~= ply then
                local can_see = gamemode.Call("PlayerCanSeePlayersChat", msg, is_team, listener, ply, is_local)
                if can_see == true then -- can be another type than a bool
                    add_to_filter(listener)
                elseif can_see == false then -- can be nil so need to check for false
                    filter[listener:AccountID() or 0] = nil
                end
            end
        end

        filter = table.ClearKeys(filter)

        net.Start(NET_BROADCAST_MSG)
        net.WriteEntity(ply)
        net.WriteString(msg)
        net.WriteBool(IsValid(ply) and (not ply:Alive()) or false)
        net.WriteBool(is_team)
        net.WriteBool(is_local)
        net.Send(filter)

        print(ply:Nick():gsub("<.->", "") .. ": " .. msg) -- shows in server console
    end)

    net.Receive(NET_SET_TYPING, function(len, ply)
        if not IsValid(ply) then return end

        local is_opened = net.ReadBool()
        ply:SetNWBool("ec_is_typing", is_opened)
        hook.Run(is_opened and "ECOpened" or "ECClosed", ply)
    end)

    function EasyChat.Init()
        hook.Run("ECPreLoadModules")
        load_modules()
        hook.Run("ECPostLoadModules")
        hook.Run("ECInitialized")
    end

    function EasyChat.PlayerCanSeePlayersChat(_, _, listener, speaker, is_local)
        if is_local then
            if not IsValid(listener) or not IsValid(speaker) then
                return false
            end
            if is_local and listener:GetPos():Distance(speaker:GetPos()) > speaker:GetInfoNum("easychat_local_msg_distance", 150) then
                return false
            end
        end
    end

    hook.Add("Initialize", TAG, EasyChat.Init)
    hook.Add("PlayerCanSeePlayersChat", TAG, EasyChat.PlayerCanSeePlayersChat)
end

if CLIENT then
    local MAX_CHARS = 3000
    local NO_COLOR = Color(0, 0, 0, 0)
    local UPLOADING_TEXT = "[uploading image...]"

    -- general
    local EC_ENABLE     = CreateClientConVar("easychat_enable",     "1", true, true, "Use easychat or not")
    local EC_NO_MODULES = CreateClientConVar("easychat_no_modules", "0", true, false, "Should easychat load modules or not")

    -- timestamps
    local EC_TIMESTAMPS = CreateClientConVar("easychat_timestamps", "1", true, false, "Display timestamp in front of messages or not")

    -- teams and colors
    local EC_TEAMS         = CreateClientConVar("easychat_teams",           "0", true, false, "Display team in front of messages or not")
    local EC_TEAMS_COLOR   = CreateClientConVar("easychat_teams_colored",   "0", true, false, "Display team with its relative color")
    local EC_PLAYER_COLOR  = CreateClientConVar("easychat_players_colored", "1", true, false, "Display player with its relative team color")
    local EC_PLAYER_PASTEL = CreateClientConVar("easychat_pastel",          "0", true, false, "Should players have pastelized colors instead of their team color")

    -- misc
    local EC_LOCAL_MSG_DIST = CreateClientConVar("easychat_local_msg_distance", "300",         true, false, "Set the maximum distance for users to receive local messages")
    local EC_TICK_SOUND     = CreateClientConVar("easychat_tick_sound",         "1",           true, false, "Should a tick sound be played on new messages or not")
    local EC_CHAT_PRINT_COL = CreateClientConVar("easychat_chatprint_color",    "151 211 255", true, false, "Chat color for certain message types")

    -- chatbox
    local EC_GLOBAL_ON_OPEN = CreateClientConVar("easychat_global_on_open",     "1",                                          true, false, "Set the chat to always open global chat tab on open")
    local EC_FONT           = CreateClientConVar("easychat_font",               system.IsWindows() and "Verdana" or "Roboto", true, false, "Set the font to use for the chat")
    local EC_FONT_SIZE      = CreateClientConVar("easychat_font_size",          "17",                                         true, false, "Set the font size for chatbox")
    local EC_HISTORY_FONT   = CreateClientConVar("easychat_history_font",       "EasyChatHistoryFont",                        true, false, "Set the font to use for the chatbox history")
    local EC_TEAM_BTN       = CreateClientConVar("easychat_team_btn",           "0",                                          true, false, "What chat should team chat button open")
    local EC_TEAM_LUA       = CreateClientConVar("easychat_team_lua",           "0",                                          true, false, "Team chat opens Lua tab")
    local EC_NEW_TEXTENTRY  = CreateClientConVar("easychat_textentryx",         "1",                                          true, false, "Use new experimental TextEntryX. Allows pasting images and other improvements. (requires Chromium supported game branch)")

    -- chathud
    local EC_HUD_FOLLOW     = CreateClientConVar("easychat_hud_follow",         "0",    true, false, "Set the chat hud to follow the chatbox")
    local EC_HUD_TTL        = CreateClientConVar("easychat_hud_ttl",            "16",   true, false, "How long messages stay before vanishing")
    local EC_HUD_SMOOTH     = CreateClientConVar("easychat_hud_smooth",         "1",    true, false, "Enables chat smoothing")
    local EC_HUD_YPOS       = CreateClientConVar("easychat_hud_ypos",           "0.79", true, false, "Position for the chat hud")

    local function StrToCol(str)
        local col = Color(255,255,255)

        local r,g,b = str:match("(%d+) (%d+) (%d+)")

        col.r = tonumber(r) or 255
        col.g = tonumber(g) or 255
        col.b = tonumber(b) or 255

        return col
    end

    cvars.AddChangeCallback(EC_ENABLE:GetName(), function(name, old, new)
        if EC_ENABLE:GetBool() then
            EasyChat.Init()
        else
            EasyChat.Destroy()
            net.Start(NET_SET_TYPING) -- this is useful if a user disable easychat with console mode
            net.WriteBool(true)
            net.SendToServer()
        end
    end)

    EasyChat.FontName = EC_FONT:GetString()
    EasyChat.FontSize = EC_FONT_SIZE:GetInt()

    local function update_chatbox_font()
        local fontname, size = EC_FONT:GetString(), EC_FONT_SIZE:GetInt()
        EasyChat.FontName = fontname
        EasyChat.FontSize = size

        surface.CreateFont("EasyChatFont", {
            font      = fontname,
            extended  = true,
            size      = size,
            weight    = 600
        })

        surface.CreateFont("EasyChatFont_Shadow", {
            font      = fontname,
            extended  = true,
            size      = size,
            weight    = 600,
            blursize  = 2
        })

        surface.CreateFont("EasyChatHistoryFont", {
            font      = fontname,
            extended  = true,
            size      = 15,
            weight    = 500,
            shadow    = false
        })
    end

    update_chatbox_font()

    cvars.AddChangeCallback("easychat_font",function(name, old, new)
        update_chatbox_font()
    end)

    cvars.AddChangeCallback("easychat_font_size",function(name, old, new)
        update_chatbox_font()
    end)

    cvars.AddChangeCallback("easychat_font_weight",function(name,old,new)
        update_chatbox_font()
    end)

    cvars.AddChangeCallback("easychat_font_shadow",function(name,old,new)
        update_chatbox_font()
    end)

    local function load_chatbox_colors()
        local JSON_COLS = file.Read("easychat/colors.txt", "DATA")

        if JSON_COLS then
            local colors = util.JSONToTable(JSON_COLS)

            EasyChat.OutlayColor        = colors.outlay
            EasyChat.OutlayOutlineColor = colors.outlayoutline
            EasyChat.TabOutlineColor    = colors.taboutline
            EasyChat.TabColor           = colors.tab
        else
            EasyChat.OutlayColor        = Color(62, 62, 62, 255)
            EasyChat.OutlayOutlineColor = Color(0, 0, 0, 0)
            EasyChat.TabColor           = Color(36, 36, 36, 255)
            EasyChat.TabOutlineColor    = Color(0, 0, 0, 0)
        end

        EasyChat.TextColor = ColorAlpha(StrToCol(EC_CHAT_PRINT_COL:GetString()), 255)
    end

    load_chatbox_colors()

    EasyChat.Mode           = 0
    EasyChat.Modes          = {}
    EasyChat.Expressions    = include("easychat/client/expressions.lua")
    EasyChat.ChatHUD        = include("easychat/client/chathud.lua")
    EasyChat.MacroProcessor = include("easychat/client/macro_processor.lua")
    EasyChat.ModeCount      = 0

    include("easychat/client/settings.lua")
    include("easychat/client/markup.lua")

    local ec_tabs    = {}
    local ec_convars = {}
    local uploading = false

    -- after easychat var declarations [necessary]
    include("easychat/client/vgui/chatbox_panel.lua")
    include("easychat/client/vgui/chat_tab.lua")
    include("easychat/client/vgui/settings_tab.lua")
    include("easychat/client/vgui/chathud_font_editor_panel.lua")

    function EasyChat.RegisterConvar(convar, desc)
        table.insert(ec_convars, {
            Convar = convar,
            Description = desc
        })
    end

    function EasyChat.GetRegisteredConvars()
        return ec_convars
    end

    function EasyChat.AddMode(name, callback)
        table.insert(EasyChat.Modes, { Name = name, Callback = callback })
        EasyChat.ModeCount = #EasyChat.Modes
    end

    function EasyChat.IsOpened()
        return EasyChat.GUI and IsValid(EasyChat.GUI.ChatBox) and EasyChat.GUI.ChatBox:IsVisible()
    end

    function EasyChat.GetDefaultBounds()
        local coef_w, coef_h = ScrW() / 2560, ScrH() / 1440
        return 50 * coef_w, ScrH() - (320 + (coef_h * 250)), 550, 320
    end

    local lastSeenNumber, lastSeenTime
    local function Check()
        local t = nil

        if input.IsKeyDown(KEY_1) then
            t = 0
        elseif input.IsKeyDown(KEY_2) then
            t = 1
        elseif input.IsKeyDown(KEY_3) then
            t = 2
        elseif input.IsKeyDown(KEY_4) then
            t = 3
        elseif input.IsKeyDown(KEY_5) then
            t = 4
        end

        if t then
            lastSeenNumber = t
            lastSeenTime = CurTime()
        end
    end

    hook.Add("Think", "EasyChat.ThinkCheckForNumber", Check)

    local function open_chatbox(is_team)
        local ok = hook.Run("ECShouldOpen")
        if ok == false then return end

        ok = hook.Run("StartChat", is_team)
        if ok == true then return end

        EasyChat.GUI.ChatBox:Show()
        EasyChat.GUI.ChatBox:MakePopup()

        Check()

        local QuickHit = false
        if lastSeenTime and lastSeenNumber and (CurTime() - lastSeenTime) < 0.75 then
            EasyChat.Mode = lastSeenNumber
            QuickHit = true

            lastSeenNumber = nil
            lastSeenTime = nil
        end

        if not QuickHit then
            EasyChat.Mode = is_team and math.Clamp(EC_TEAM_BTN:GetInt() + 1, 1, EasyChat.ModeCount) or 0
        end

        if EC_GLOBAL_ON_OPEN:GetBool() then
            if EC_TEAM_LUA:GetBool() and is_team then
                EasyChat.OpenTab("Lua")
            else
                EasyChat.OpenTab("Global")
                EasyChat.GUI.TextEntry:RequestFocus()
                timer.Simple(0, function()
                    EasyChat.GUI.RichText:GotoTextEnd()
                end)
            end
        end

        EasyChat.GUI.TextEntry:SetText("")

        hook.Run("ECOpened", LocalPlayer())

        net.Start(NET_SET_TYPING)
        net.WriteBool(true)
        net.SendToServer()
    end

    local function save_chatbox_bounds()
        local x, y, w, h = EasyChat.GUI.ChatBox:GetBounds()
        cookie.Set("EasyChat_PosX", x)
        cookie.Set("EasyChat_PosY", y)
        cookie.Set("EasyChat_Width", w)
        cookie.Set("EasyChat_Height", h)
    end

    local function load_chatbox_bounds()
        if not cookie.GetNumber("EasyChat_FirstRun") or cookie.GetNumber("EasyChat_FirstRun") == 0 then
            if file.Exists("easychat/possize.txt","DATA") then
                local tbl = util.JSONToTable(file.Read("easychat/possize.txt", "DATA"))
                cookie.Set("EasyChat_Width", tbl.w)
                cookie.Set("EasyChat_Height", tbl.h)
                cookie.Set("EasyChat_PosX", tbl.x)
                cookie.Set("EasyChat_PosY", tbl.y)
                cookie.Set("EasyChat_FirstRun", 1)
            else
                cookie.Set("EasyChat_Width", 550)
                cookie.Set("EasyChat_Height", 320)
                cookie.Set("EasyChat_PosX", 25)
                cookie.Set("EasyChat_PosY", (ScrH() * 0.79) - 320)
                cookie.Set("EasyChat_FirstRun", 1)
            end
        end

        local w,h = cookie.GetNumber("EasyChat_Width", 550), cookie.GetNumber("EasyChat_Height", 320)
        local x,y = cookie.GetNumber("EasyChat_PosX", 25), cookie.GetNumber("EasyChat_PosY", (ScrH() * 0.79) - 320)

        if w >= ScrW() then
            w = ScrW()
            cookie.Set("EasyChat_Width", w)
        end
        if h >= ScrH() then
            h = ScrH()
            cookie.Set("EasyChat_Height", h)
        end
        if x >= ScrW() or x + w >= ScrW() then
            x = ScrW() - w
            cookie.Set("EasyChat_PosX", x)
        end
        if y >= ScrH() or y + h >= ScrH() then
            y = ScrH() - h
            cookie.Set("EasyChat_PosY", y)
        end
        if x < 0 then
            x = 0
            cookie.Set("EasyChat_PosX", x)
        end
        if y < 0 then
            y = 0
            cookie.Set("EasyChat_PosY", y)
        end
        return x, y, w, h
    end

    local function close_chatbox()
        if not EasyChat.IsOpened() then return end

        EasyChat.GUI.ChatBox:SetMouseInputEnabled(false)
        EasyChat.GUI.ChatBox:SetKeyboardInputEnabled(false)
        EasyChat.GUI.TextEntry:SetText("")

        gui.EnableScreenClicker(false)
        chat.old_Close()
        gamemode.Call("ChatTextChanged", "")
        gamemode.Call("FinishChat")

        save_chatbox_bounds()
        EasyChat.GUI.ChatBox:Hide()

        hook.Run("ECClosed", LocalPlayer())

        net.Start(NET_SET_TYPING)
        net.WriteBool(false)
        net.SendToServer()
    end

    function EasyChat.IsURL(str)
        local patterns = {
            "https?://[^%s%\"%>%<]+",
            "ftp://[^%s%\"%>%<]+",
            "steam://[^%s%\"%>%<]+",
            "www%.[^%s%\"]+%.[^%s%\"]+"
        }

        for _, pattern in ipairs(patterns) do
            local start_pos, end_pos = str:find(pattern, 1, false)
            if start_pos then
                return start_pos, end_pos
            end
        end

        return false
    end

    function EasyChat.OpenURL(url)
        local has_protocol = url:find("^%w-://")
        if not has_protocol then
            url = ("http://%s"):format(url)
        end

        local ok = hook.Run("ECOpenURL", url)
        if ok == false then return end

        gui.OpenURL(url)
    end

    function EasyChat.AskForInput(title, callback, can_be_empty)
        local frame = vgui.Create("DFrame")
        frame:SetTitle(title)
        frame:SetSize(200,110)
        frame:Center()
        frame.OldPaint = frame.Paint
        frame.Paint = function(self, w, h)
            Derma_DrawBackgroundBlur(self, 0)

            self:OldPaint(w, h)
        end

        local text_entry = frame:Add("DTextEntry")
        text_entry:SetSize(180, 25)
        text_entry:SetPos(10, 40)
        text_entry.OnEnter = function(self)
            if not can_be_empty and self:GetText():Trim() == "" then return end

            callback(self:GetText())
            frame:Close()
        end

        local btn = frame:Add("DButton")
        btn:SetText("OK")
        btn:SetSize(100, 25)
        btn:SetPos(50, 75)
        btn.DoClick = function()
            if not can_be_empty and text_entry:GetText():Trim() == "" then return end

            callback(text_entry:GetText())
            frame:Close()
        end

        frame:MakePopup()
        text_entry:RequestFocus()
    end

    local BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    function EasyChat.DecodeBase64(base64)
        base64 = string.gsub(base64, "[^" .. BASE64 .. "=]", "")
        return (base64:gsub(".", function(x)
            if (x == "=") then return "" end
            local r, f = "", (BASE64:find(x) - 1)
            for i = 6, 1, -1 do
                r = r .. (f % 2^i - f % 2^(i - 1) > 0 and "1" or "0")
            end

            return r
        end):gsub("%d%d%d?%d?%d?%d?%d?%d?", function(x)
            if (#x ~= 8) then return "" end
            local c = 0
            for i=1, 8 do
                c = c + (x:sub(i, i) == "1" and 2^(8 - i) or 0)
            end

            return string.char(c)
        end))
    end

    local function on_imgur_failure(err)
        EasyChat.Print(true, ("imgur upload failed: %s"):format(tostring(err)))
    end

    local function on_imgur_success(code, body, headers)
        if code ~= 200 then
            on_imgur_failure(("error code: %d"):format(code))
            return
        end

        local decoded_body = util.JSONToTable(body)
        if not decoded_body then
            on_imgur_failure("could not json decode body")
            return
        end

        if not decoded_body.success then
            on_imgur_failure(("%s: %s"):format(
                decoded_body.status or "unknown status?",
                decoded_body.data and decoded_body.data.error or "unknown error"
            ))
            return
        end

        local url = decoded_body.data and decoded_body.data.link
        if not url then
            on_imgur_failure("success but link wasn't found?")
            return
        end

        EasyChat.Print(("imgur uploaded: %s"):format(tostring(url)))
        return url
    end

    function EasyChat.UploadToImgur(img_base64, callback)
        local ply_nick, ply_steamid = LocalPlayer():Nick(), LocalPlayer():SteamID()
        local params = {
            image = img_base64,
            type = "base64",
            name = tostring(os.time()),
            title = ("%s - %s"):format(ply_nick, ply_steamid),
            description = ("%s (%s) on %s"):format(ply_nick, ply_steamid, os.date("%d/%m/%Y at %H:%M")),
        }

        local headers = {}
        headers["Authorization"] = "Client-ID 62f1e31985e240b"

        local http_data = {
            failed = function(...) on_imgur_failure(...) on_finished(nil) end,
            success = function(...) local url = on_imgur_success(...) callback(url) end,
            method = "post",
            url = "https://api.imgur.com/3/image.json",
            parameters = params,
            headers = headers,
        }

        HTTP(http_data)
        EasyChat.Print(("sent picture (%s) to imgur"):format(string.NiceSize(#img_base64)))
    end

local emote_lookup_tables = {}
    function EasyChat.AddEmoteLookupTable(loookup_name, lookup_table)
        emote_lookup_tables[loookup_name] = lookup_table
    end

    function EasyChat.GetEmoteLookupTable(lookup_name)
        return emote_lookup_tables[lookup_name] or {}
    end

    function EasyChat.GetEmoteLookupTables()
        return emote_lookup_tables
    end

    local ec_addtext_handles = {}
    function EasyChat.SetAddTextTypeHandle(type, callback)
        ec_addtext_handles[type] = callback
    end

    function EasyChat.GetSetAddTextTypeHandle(type)
        return ec_addtext_handles[type]
    end

    function EasyChat.Init()
        load_chatbox_colors()

        -- reset for reload
        EasyChat.Mode           = 0
        EasyChat.Modes          = {}
        EasyChat.Expressions    = include("easychat/client/expressions.lua")
        EasyChat.ChatHUD        = include("easychat/client/chathud.lua")
        EasyChat.MacroProcessor = include("easychat/client/macro_processor.lua")
        EasyChat.ModeCount      = 0

        include("easychat/client/settings.lua")
        include("easychat/client/markup.lua")

        ec_convars         = {}
        ec_addtext_handles = {}
        uploading = false

        function EasyChat.SendGlobalMessage(msg, is_team, is_local)
            msg = EasyChat.MacroProcessor:ProcessString(msg:sub(1, MAX_CHARS))

            net.Start(NET_SEND_MSG)
            net.WriteString(msg)
            net.WriteBool(is_team)
            net.WriteBool(is_local)
            net.SendToServer()
        end

        EasyChat.AddMode("Team", function(text)
            EasyChat.SendGlobalMessage(text, true, false)
        end)

        EasyChat.AddMode("Local", function(text)
            EasyChat.SendGlobalMessage(text, false, true)
        end)

        EasyChat.AddMode("Console", function(text)
            LocalPlayer():ConCommand(text)
        end)

        local function append_text(richtext, text)
            if richtext.HistoryName then
                richtext.Log = richtext.Log and richtext.Log .. text or text
            end

            richtext:AppendText(text)
        end

        local function append_text_url(richtext, text)
            local start_pos, end_pos = EasyChat.IsURL(text)
            if not start_pos then
                append_text(richtext, text)
            else
                local url = text:sub(start_pos, end_pos)
                append_text(richtext, text:sub(1, start_pos - 1))
                richtext:InsertClickableTextStart(url)
                append_text(richtext, url)
                richtext:InsertClickableTextEnd()

                -- recurse for possible other urls after this one
                append_text_url(richtext, text:sub(end_pos + 1))
            end
        end

        local function save_text(richtext)
            if not richtext.HistoryName then return end

            EasyChat.SaveToHistory(richtext.HistoryName, richtext.Log)
            richtext.Log = ""
        end

        local function global_append_text(text)
            EasyChat.ChatHUD:AppendText(text)
            append_text(EasyChat.GUI.RichText, text)
        end

        local image_url_patterns = {
            "^https?://steamuserimages%-a%.akamaihd%.net/ugc/[0-9]+/[A-Z0-9]+/",
            "^https?://pbs%.twimg%.com/media/",
        }
        local image_url_exts = { "png", "jpg", "jpeg", "gif", "gifv", "webp" }
        local function is_image_url(url)
            local simple_url = url:gsub("%?[^/]+", ""):lower() -- remove url args, lower for exts like JPG, PNG
            for _, url_ext in ipairs(image_url_exts) do
                local pattern = (".%s$"):format(url_ext)
                if simple_url:match(pattern) then return true end
            end

            for _, pattern in ipairs(image_url_patterns) do
                if url:match(pattern) then return true end
            end

            return false
        end

        local function global_append_text_url(text)
            local start_pos, end_pos = EasyChat.IsURL(text)
            if not start_pos then
                global_append_text(text)
            else
                local url = text:sub(start_pos, end_pos)
                global_append_text(text:sub(1, start_pos - 1))

                if is_image_url(url) then
                    EasyChat.ChatHUD:AppendImageURL(url)
                    EasyChat.GUI.RichText:InsertClickableTextStart(url)
                    append_text(EasyChat.GUI.RichText, url)
                    EasyChat.GUI.RichText:InsertClickableTextEnd()
                else
                    EasyChat.GUI.RichText:InsertClickableTextStart(url)
                    global_append_text(url)
                    EasyChat.GUI.RichText:InsertClickableTextEnd()
                end

                -- recurse for possible other urls after this one
                global_append_text_url(text:sub(end_pos + 1))
            end
        end

        local function global_append_nick(str)
            if not ec_markup then
                append_text(EasyChat.GUI.RichText, str)
            else
                -- use markup to get text and colors out of nicks
                local mk = ec_markup.Parse(str, nil, true)
                for _, line in ipairs(mk.Lines) do
                    for _, component in ipairs(line.Components) do
                        if component.Color then
                            local c = component.Color
                            EasyChat.GUI.RichText:InsertColorChange(c.r, c.g, c.b, 255)
                        elseif component.Type == "text" then
                            append_text(EasyChat.GUI.RichText, component.Content)
                        end
                    end
                end
            end

            EasyChat.GUI.RichText:InsertColorChange(255, 255, 255, 255)

            -- let the chathud do its own thing
            local chathud = EasyChat.ChatHUD
            chathud:AppendNick(str)
            chathud:PushPartComponent("stop")
        end

        local function global_insert_color_change(r, g, b, a)
            EasyChat.GUI.RichText:InsertColorChange(r, g, b, a)
            EasyChat.ChatHUD:InsertColorChange(r, g, b)
        end

        EasyChat.SetAddTextTypeHandle("table", function(col)
            global_insert_color_change(col.r or 255, col.g or 255, col.b or 255, col.a or 255)
        end)

        EasyChat.SetAddTextTypeHandle("string", global_append_text_url)

        local function string_hash(text)
            local counter = 1
            local len = string.len(text)
            for i = 1, len, 3 do
                counter =
                    math.fmod(counter * 8161, 4294967279) + -- 2^32 - 17: Prime!
                    (text:byte(i) * 16776193) +
                    ((text:byte(i + 1) or (len - i + 256)) * 8372226) +
                    ((text:byte(i + 2) or (len - i + 256)) * 3932164)
            end

            return math.fmod(counter, 4294967291) -- 2^32 - 5: Prime (and different from the prime in the loop)
        end

        local function pastelize_nick(nick, small_seed)
            local hue = string_hash(nick) + (small_seed or 0)
            local saturation, value = hue % 3 == 0, hue % 127 == 0
            return HSVToColor(hue % 180 * 2, saturation and 0.3 or 0.6, value and 0.6 or 1)
        end

        EasyChat.SetAddTextTypeHandle("Player", function(ply)
            local tag = ply.aTags_ch or {}
            local pieces, messageColor, nameColor = tag.pieces, tag.messageColor, tag.nameColor

            local col = EC_PLAYER_COLOR:GetBool() and team.GetColor(ply:Team()) or Color(255,255,255)
            if EC_PLAYER_PASTEL:GetBool() then
                col = pastelize_nick(ply.DecoratedNick and ply:DecoratedNick() or ply:Nick())
            end
            if nameColor then
                col = nameColor
            end

            global_insert_color_change(col.r, col.g, col.b, 255)
            global_append_nick(ply.DecoratedNick and ply:DecoratedNick() or ply:Nick())
        end)

        function EasyChat.AddText(richtext, ...)
            append_text(richtext, "\n")

            local args = {...}
            for _, arg in ipairs(args) do
                if isstring(arg) then
                    append_text_url(richtext, arg)
                elseif type(arg) == "Player" then
                    -- this can happen if the function is ran early
                    if ec_markup then
                        local ply_nick = ec_markup.Parse(arg.DecoratedNick and arg:DecoratedNick() or arg:Nick()):GetText()
                        append_text(richtext, ply_nick)
                    else
                        append_text(richtext, arg.DecoratedNick and arg:DecoratedNick() or arg:Nick())
                    end
                elseif type(arg) == "table" then
                    richtext:InsertColorChange(arg.r or 255, arg.g or 255, arg.b or 255, arg.a or 255)
                end
            end
        end

        do
            chat.old_AddText        = chat.old_AddText        or chat.AddText
            chat.old_GetChatBoxPos  = chat.old_GetChatBoxPos  or chat.GetChatBoxPos
            chat.old_GetChatBoxSize = chat.old_GetChatBoxSize or chat.GetChatBoxSize
            chat.old_Open           = chat.old_Open           or chat.Open
            chat.old_Close          = chat.old_Close          or chat.Close

            function chat.AddTimestamp(tbl)
                if EC_ENABLE:GetBool() and EC_TIMESTAMPS:GetBool() then
                    tbl = tbl or {}

                    table.insert(tbl, 1, " - ")
                    table.insert(tbl, 1, Color(255,255,255))
                    table.insert(tbl, 1, os.date("%H:%M"))
                    table.insert(tbl, 1, Color(119,171,218))
                end
                return tbl
            end

            function chat.AddText(...)
                EasyChat.ChatHUD:NewLine()
                append_text(EasyChat.GUI.RichText, "\n")
                local col = StrToCol(EC_CHAT_PRINT_COL:GetString())
                global_insert_color_change(col.r, col.g, col.b, 255)

                local args = {...}
                local _args = {}
                for _, arg in ipairs(args) do
                    local callback = ec_addtext_handles[type(arg)]
                    if callback then
                        pcall(callback, arg)
                    else
                        local str = tostring(arg)
                        global_append_text(str)
                    end

                    -- regular chat.AddText hack with nicks
                    if isentity(arg) and IsValid(arg) and arg:IsPlayer() then
                        table.insert(_args, team.GetColor(arg:Team()))
                        table.insert(_args, arg:Nick())
                    else
                        table.insert(_args, arg)
                    end
                end

                EasyChat.ChatHUD:PushPartComponent("stop")
                EasyChat.ChatHUD:InvalidateLayout()

                chat.old_AddText(col, unpack(_args))

                if EC_TICK_SOUND:GetBool() then
                    chat.PlaySound()
                end
            end

            function chat.GetChatBoxPos()
                if EasyChat.GUI and IsValid(EasyChat.GUI.ChatBox) then
                    local x, y, _, _ = EasyChat.GUI.ChatBox:GetBounds()
                    return x, y
                else
                    return chat.old_GetChatBoxPos()
                end
            end

            function chat.GetChatBoxSize()
                if EasyChat.GUI and IsValid(EasyChat.GUI.ChatBox) then
                    local _, _, w, h = EasyChat.GUI.ChatBox:GetBounds()
                    return w, h
                else
                    return chat.old_GetChatBoxSize()
                end
            end

            function chat.Open(inp)
                local is_team = inp == 0
                open_chatbox(is_team)
                --chat.old_Open(inp)
            end

            chat.Close = close_chatbox
        end

        do
            local chatbox_frame = vgui.Create("ECChatBox")
            local cx, cy, cw, ch = load_chatbox_bounds()
            chatbox_frame:SetSize(cw, ch)
            chatbox_frame:SetPos(cx, cy)
            chatbox_frame.btnClose.DoClick = close_chatbox
            chatbox_frame.btnClose.DoRightClick = function(s)
                local m = DermaMenu()
                local close = m:AddOption("Close", close_chatbox)
                close:SetIcon("icon16/cross.png")
                local reload = m:AddOption("Reload EasyChat", function()
                    RunConsoleCommand("easychat_reload")
                end)
                reload:SetIcon("icon16/arrow_refresh.png")
                m:Open()
            end

            function chatbox_frame.Tabs:OnActiveTabChanged(old_tab, new_tab)
                hook.Run("ECTabChanged", old_tab.Name, new_tab.Name)
            end

            function EasyChat.AddTab(name, panel, icon)
                local tab = chatbox_frame.Tabs:AddSheet(name, panel, icon or nil)
                tab.Tab.Name = name
                ec_tabs[name] = tab
                panel:Dock(FILL)
            end

            function EasyChat.OpenTab(name)
                chatbox_frame.Tabs:SwitchToName(name)
            end

            function EasyChat.GetTab(name)
                if ec_tabs[name] then
                    return ec_tabs[name]
                else
                    return nil
                end
            end

            function EasyChat.GetActiveTab()
                local active = chatbox_frame.Tabs:GetActiveTab()
                return ec_tabs[active.Name]
            end

            function EasyChat.SetFocusForOn(name, panel)
                if ec_tabs[name] then
                    ec_tabs[name].Tab.FocusOn = panel
                end
            end

            function EasyChat.FlashTab(name)
                if ec_tabs[name] then
                    ec_tabs[name].Tab.Flashed = true
                end
            end

            local global_tab = vgui.Create("ECChatTab")
            EasyChat.AddTab("Global", global_tab, "icon16/comments.png")
            EasyChat.SetFocusForOn("Global", global_tab.TextEntry)

            -- Only the neccesary elements --
            EasyChat.GUI = {
                ChatBox    = chatbox_frame,
                TextEntry  = global_tab.TextEntry,
                RichText   = global_tab.RichText,
                TabControl = chatbox_frame.Tabs
            }

            hook.Add("Think", TAG, function()
                local chathud = EasyChat.ChatHUD
                if not chathud then return end

                if EC_HUD_FOLLOW:GetBool() then
                    local x, y, w, h = chatbox_frame:GetBounds()
                    chathud.Pos = {X = x, Y = y}
                    chathud.Size = {W = w, H = h}
                else
                    chathud.Pos = {X = 24 + ((pace and pace.IsActive() and IsValid(pace.Editor) and pace.Editor:GetAlpha() ~= 0) and pace.Editor:GetWide() or 0), Y = ScrH() * EC_HUD_YPOS:GetFloat() - 320}
                    chathud.Size = {W = (ScrW() / 2) - 24, H = 320}
                end
            end)

            close_chatbox()
        end

        local ctrl_shortcuts = {}
        local alt_shortcuts  = {}

        local invalid_shortcut_keys = {
            KEY_ENTER     = true,
            KEY_PAD_ENTER = true,
            KEY_ESCAPE    = true,
            KEY_TAB       = true
        }
        local function is_valid_shortcut_key(key)
            return invalid_shortcut_keys[key] and true or false
        end

        local valid_base_keys = {
            KEY_LCONTROL = true,
            KEY_LALT     = true,
            KEY_RCONTROL = true,
            KEY_RALT     = true
        }
        local function is_base_shortcut_key(key)
            return valid_base_keys[key] and true or false
        end

        function EasyChat.RegisterCTRLShortcut(key, callback)
            if is_valid_shortcut_key(key) then
                ctrl_shortcuts[key] = callback
            end
        end

        function EasyChat.RegisterALTShortcut(key, callback)
            if not is_valid_shortcut_key(key) then
                alt_shortcuts[key] = callback
            end
        end

        --[[
            /!\ Not used for the main chat text entry
            Its because the chat text entry is a DHTML
            panel that does not fire OnKeyCode* callbacks
        ]]--
        function EasyChat.UseRegisteredShortcuts(text_entry, last_key, key)
            if is_base_shortcut_key(last_key) then
                local pos = text_entry:GetCaretPos()
                local first = text_entry:GetText():sub(1, pos + 1)
                local last = text_entry:GetText():sub(pos + 2, #text_entry:GetText())

                if ctrl_shortcuts[key] then
                    local retrieved = ctrl_shortcuts[key](text_entry, text_entry:GetText(), pos, first, last)
                    if retrieved then
                        text_entry:SetText(retrieved)
                    end
                elseif alt_shortcuts[key] then
                    local retrieved = alt_shortcuts[key](text_entry, text_entry:GetText(), pos, first, last)
                    if retrieved then
                        text_entry:SetText(retrieved)
                    end
                end
            end
        end

        --[[
            /!\ Not used for the main chat text entry
            Its because the chat text entry is a DHTML
            panel that does not fire OnKeyCode* callbacks
        ]]--
        function EasyChat.SetupHistory(text_entry, key)
            if key == KEY_ENTER or key == KEY_PAD_ENTER then
                text_entry:AddHistory(text_entry:GetText())
                text_entry.HistoryPos = 0
            end

            if key == KEY_ESCAPE then
                text_entry.HistoryPos = 0
            end

            if not text_entry.HistoryPos then return end

            if key == KEY_UP then
                text_entry.HistoryPos = text_entry.HistoryPos - 1
                text_entry:UpdateFromHistory()
            elseif key == KEY_DOWN then
                text_entry.HistoryPos = text_entry.HistoryPos + 1
                text_entry:UpdateFromHistory()
            end
        end

        function EasyChat.GUI.TextEntry:OnTab()
            if self:GetText() ~= "" then
                local autocompletion_text = gamemode.Call("OnChatTab", self:GetText())
                self:SetText(autocompletion_text)
                timer.Simple(0, function()
                    self:RequestFocus()
                end)
            else
                local next_mode = EasyChat.Mode + 1
                EasyChat.Mode = next_mode > EasyChat.ModeCount and 0 or next_mode
            end
        end

        function EasyChat.GUI.TextEntry:OnEnter()
            self:SetText(self:GetText():Replace("╚​", ""))
            if self:GetText():Trim() ~= "" then
                if EasyChat.Mode == 0 then
                    EasyChat.SendGlobalMessage(self:GetText(), false, false)
                else
                    local mode = EasyChat.Modes[EasyChat.Mode]
                    mode.Callback(self:GetText())
                end
            end

            close_chatbox()
        end

        function EasyChat.GUI.TextEntry:OnImagePaste(name, base64)
            if uploading then return end

            self:SetText(self:GetText() .. UPLOADING_TEXT)
            uploading = true

            EasyChat.UploadToImgur(base64, function(url)
                if not url then
                    local cur_text = self:GetText():Trim()
                    if cur_text:match(UPLOADING_TEXT) then
                        self:SetText(cur_text:Replace(UPLOADING_TEXT, ""))
                    end

                    notification.AddLegacy("Image upload failed, check your console", NOTIFY_ERROR, 3)
                    surface.PlaySound("buttons/button11.wav")
                else
                    local cur_text = self:GetText():Trim()
                    if cur_text:match(UPLOADING_TEXT) then
                        self:SetText(cur_text:Replace(UPLOADING_TEXT, url))
                    end
                end

                uploading = false
            end)
        end

        function EasyChat.GUI.TextEntry:OnValueChange(text)
            gamemode.Call("ChatTextChanged", text)
        end

        function EasyChat.GUI.RichText:ActionSignal(name, value)
            if name ~= "TextClicked" then return end
            EasyChat.OpenURL(value)
        end

        local invalid_chat_keys = {
            [KEY_LCONTROL] = true, [KEY_LALT] = true,
            [KEY_RCONTROL] = true, [KEY_RALT] = true,
        }
        local function is_chat_key_pressed(key_code)
            if invalid_chat_keys[key_code] then return false end
            -- above 0 so we dont include KEY_NONE/KEY_FIRSt, under 67 because above are control keys
            if key_code > 0 and key_code <= 67 then return true end

            return false
        end

        function EasyChat.GUI.ChatBox:OnKeyCodePressed(key_code)
            if not EasyChat.IsOpened() then return end

            local tab = EasyChat.GUI.TabControl:GetActiveTab()
            if tab.FocusOn and not tab.FocusOn:HasFocus() then
                if is_chat_key_pressed(key_code) then
                    local key_name = input.GetKeyName(key_code)
                    if key_name == "ENTER" or key_name == "TAB" then
                        key_name = ""
                    end

                    local cur_text = tab.FocusOn:GetText()
                    tab.FocusOn:RequestFocus()
                    tab.FocusOn:SetText(cur_text .. key_name)
                    tab.FocusOn:SetCaretPos(#tab.FocusOn:GetText())
                end
            end
        end

        hook.Add("PlayerBindPress", TAG, function(ply, bind, pressed)
            if ply ~= LocalPlayer() then return end
            if ply.Instrument and IsValid(ply.Instrument) and not input.IsKeyDown(KEY_LALT) then return end
            if not pressed then return end

            if bind == "messagemode" then
                open_chatbox(false)
                return true
            elseif bind == "messagemode2" then
                open_chatbox(true)
                return true
            end
        end)

        hook.Add("PreRender", TAG, function()
            if not EasyChat.IsOpened() then return end
            if not input.IsKeyDown(KEY_ESCAPE) then return end

            close_chatbox()
            gui.HideGameUI()
        end)

        -- we do that here so its available from modules
        local settings = vgui.Create("ECSettingsTab")
        EasyChat.Settings = settings

        hook.Run("ECPreLoadModules")

        if not EC_NO_MODULES:GetBool() then
            load_modules()
        end

        hook.Run("ECPostLoadModules")

        EasyChat.AddTab("Settings", settings, "icon16/wrench_orange.png")

        local chat_text_types = {
            none = true,         -- fallback
            darkrp = true,       -- darkrp compat most likely?
            namechange = true,   -- annoying
            servermsg = true,    -- annoying
            --teamchange = true, -- annoying
            --chat = true,       -- deprecated
        }
        hook.Add("ChatText", TAG, function(index, name, text, type)
            if not chat_text_types[type] then return end

            chat.AddText(text)
        end)

        hook.Add("HUDShouldDraw", TAG, function(hud_element)
            if hud_element == "CHudChat" then return false end
        end)

        local chathud = EasyChat.ChatHUD
        local x, y, w, h = EasyChat.GetDefaultBounds()
        chathud.Pos = {X = x, Y = y}
        chathud.Size = {W = w, H = h}
        chathud:InvalidateLayout()

        hook.Add("HUDPaint", TAG, function()
            chathud:Draw()
        end)

        -- for getting rid of chathud related annoying stuff
        hook.Add("OnPlayerChat", TAG, function(ply, text)
            if text == "sh" or text:match("%ssh%s") then
                chathud:StopComponents()
            end
        end)

        hook.Run("ECInitialized")
    end

    net.Receive(NET_BROADCAST_MSG, function()
        local ply      = net.ReadEntity()
        local msg      = net.ReadString()
        local dead     = net.ReadBool()
        local is_team  = net.ReadBool()
        local is_local = net.ReadBool()

        -- so we never have the two together
        if is_local and is_team then
            is_team = false
        end

        gamemode.Call("OnPlayerChat", ply, msg, is_team, dead, is_local)
    end)

    hook.Add("Initialize", TAG, function()
        if EC_ENABLE:GetBool() then
            EasyChat.Init()
        end

        -- this is for the best
        function GAMEMODE:OnPlayerChat(ply, msg, is_team, is_dead, is_local)
            local msg_components = {}

            -- reset color to white
            table.insert(msg_components, Color(255, 255, 255))

            if EC_ENABLE:GetBool() and IsValid(ply) and EC_TEAMS:GetBool() then
                if EC_TEAMS_COLOR:GetBool() then
                    local tcol = team.GetColor(ply:Team())
                    table.insert(msg_components, tcol)
                end
                table.insert(msg_components, "[" .. team.GetName(ply:Team()):gsub("^.", string.upper) .. "] ")
            end

            if IsValid(ply) and ply:Team() == TEAM_SPECTATOR then
                table.insert(msg_components, Color(192, 192, 192))
                table.insert(msg_components, "*SPEC* ")
            end

            if is_dead and IsValid(ply) and ply:Team() ~= TEAM_SPECTATOR then
                table.insert(msg_components, Color(240, 80, 80))
                table.insert(msg_components, "*DEAD* ")
            end

            if is_local == true then
                table.insert(msg_components, Color(30, 160, 40))
                table.insert(msg_components, "(Local) ")
            end

            if is_team then
                table.insert(msg_components, Color(30, 160, 40))
                table.insert(msg_components, "(TEAM) ")
            end

            if pieces and #pieces > 0 then
                for k, v in pairs(pieces) do
                    table.insert(msg_components, v.color or Color(255, 255, 255))
                    table.insert(msg_components, v.name or "")
                end
            end

            if IsValid(ply) then
                table.insert(msg_components, ply)
            else
                table.insert(msg_components, Color(110, 247, 177))
                table.insert(msg_components, "Unknown")
            end

            local atags = ply.aTags_ch or {}
            local pieces, messageColor, nameColor = atags.pieces, atags.messageColor, atags.nameColor

            if messageColor and messageColor ~= "" then
                table.insert(msg_components, messageColor)
            else
                table.insert(msg_components, Color(255, 255, 255))
            end
            table.insert(msg_components, ": " .. msg)

            if chat.AddTimestamp then
                chat.AddTimestamp(msg_components)
            end

            chat.AddText(unpack(msg_components))

            return true
        end

        hook.Run("ECPostInitialize")
    end)

    concommand.Add("say_local", function(ply, cmd, args, argStr)
        if not IsValid(ply) or ply ~= LocalPlayer() then return end
        if not args then return end

        net.Start(NET_SEND_MSG)
        net.WriteString(argStr:sub(1, MAX_CHARS))
        net.WriteBool(false)
        net.WriteBool(true)
        net.SendToServer()
    end)
end

function EasyChat.Destroy()
    -- dont fuck destroying if your addon is bad
    local succ, err = pcall(hook.Run, "ECPreDestroy")
    if not succ then
        ErrorNoHalt(err)
    end

    if CLIENT then
        hook.Remove("PreRender", TAG)
        hook.Remove("Think", TAG)
        hook.Remove("PlayerBindPress", TAG)
        hook.Remove("HUDShouldDraw", TAG)
        hook.Remove("OnPlayerChat", TAG)
        hook.Remove("Think", "EasyChat.ThinkCheckForNumber")

        if chat.old_AddText then
            chat.AddText        = chat.old_AddText
            chat.GetChatBoxPos  = chat.old_GetChatBoxPos
            chat.GetChatBoxSize = chat.old_GetChatBoxSize
            chat.Open           = chat.old_Open
            chat.Close          = chat.old_Close
        end

        EasyChat.ModeCount = 0
        EasyChat.Mode = 0
        EasyChat.Modes = {}

        if EasyChat.GUI and IsValid(EasyChat.GUI.ChatBox) then
            EasyChat.GUI.ChatBox:Remove()
        end

        if EasyChat.ChatHUD then
            EasyChat.ChatHUD:Clear()
        end
    end

    hook.Run("ECDestroyed")
end

function EasyChat.Reload()
    EasyChat.Destroy()
    EasyChat.Init()

    if SERVER then
        for _, v in ipairs(player.GetAll()) do
            v:SendLua([[EasyChat.Reload()]])
        end
    end
end

concommand.Add("easychat_reload", EasyChat.Reload)