local nick_cache = {}
local function cache_nick(ply)
    local nick, team_color = ply.DecoratedNick and ply:DecoratedNick() or ply:Nick(), team.GetColor(ply:Team())
    local cache = nick_cache[nick]
    if cache and cache.DefaultColor == team_color then
        return cache
    end

    local mk = ec_markup.Parse(nick, nil, true, team_color, "EasyChatFont")
    nick_cache[nick] = mk

    return mk
end

local dist = GetConVar("easychat_local_msg_distance")
local plys = {}
hook.Add("PostRenderVGUI", "EasyChatModuleLocalUI", function()
    if not IsValid(EasyChat.GUI.ChatBox) then return end
    local mainGUI = EasyChat.GUI.ChatBox

    local cur_mode = EasyChat.Modes[EasyChat.Mode]
    if not EasyChat.IsOpened() or not cur_mode or (cur_mode and cur_mode.Name ~= "Local") then return end

    for _, pl in pairs(player.GetAll()) do
        if pl == LocalPlayer() then continue end
        if pl:GetPos():Distance(LocalPlayer():GetPos()) <= dist:GetInt() then
            table.insert(plys, pl)
        end
    end

    local x, y = mainGUI:GetPos()
    y = y + mainGUI:GetTall()

    local w, h = 0, 0

    surface.SetFont("EasyChatFont")
    local tw, th = surface.GetTextSize("Players that can hear this:")
    w = math.max(w, tw)
    h = (#plys + 1) * th

    x = x - w
    if x < 0 then
        x = x + w + mainGUI:GetWide()
    end
    y = y - h - (th * 2)

    surface.SetDrawColor(66, 66, 66, 100)
    surface.DrawRect(x, y, w, h)
    surface.SetDrawColor(66, 66, 66, 150)
    surface.DrawOutlinedRect(x, y, w, h)

    draw.SimpleText("Players that can hear this:", "EasyChatFont", x, y, Color(192, 0, 0), TEXT_ALIGN_LEFT)

    for i, name in pairs(plys) do
        local mk = cache_nick(ply)
        mk:Draw(x, y + (th * i))
    end
end)

hook.Add("ECDestroyed", "EasyChatModuleLocalUI", function()
    hook.Remove("PostRenderVGUI", "EasyChatModuleLocalUI")
end)

return "Local Message UI"