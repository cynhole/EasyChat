-- audio
local snd_mute_losefocus = GetConVar("snd_mute_losefocus")
local chatsounds = GetConVar("chatsounds_enabled")
local chatsounds_autocomplete_ui = GetConVar("chatsounds_autocomplete_ui")
local playx_volume = GetConVar("playx_volume")
local mediaplayer_volume = GetConVar("mediaplayer_volume")
local pac_ogg_volume = GetConVar("pac_ogg_volume")

-- graphics
local cl_drawownshadow = GetConVar("cl_drawownshadow")
local r_shadows = GetConVar("r_shadows")
local cl_playerspraydisable = GetConVar("cl_playerspraydisable")
local r_3dsky = GetConVar("r_3dsky")
local r_WaterDrawReflection = GetConVar("r_WaterDrawReflection")
local r_WaterDrawRefraction = GetConVar("r_WaterDrawRefraction")

local function create_option_set(settings, category_name, options)
    for cvar, description in pairs(options) do
        settings:AddConvarSetting(category_name, "boolean", cvar, description)
    end

    local setting_reset_options = settings:AddSetting(category_name, "action", "Reset Options")
    setting_reset_options.DoClick = function()
        for cvar, _ in pairs(options) do
            local default_value = tobool(cvar:GetDefault())
            cvar:SetBool(default_value)
        end
    end
end

local function audio_settings()
    local settings = EasyChat.Settings
    local category_name = "Audio"

    local options = {
        [snd_mute_losefocus] = "Out of game mute",
    }

    if chatsounds then
        options[chatsounds] = "Chatsounds"
    end
    if chatsounds_autocomplete_ui then
        options[chatsounds_autocomplete_ui] = "Chatsounds: Autocomplete UI"
    end

    create_option_set(settings, category_name, options)

    if playx_volume then
        settings:AddConvarSetting(category_name, "float", playx_volume, "PlayX Media Volume", 0, 100, 0)
    end
    if mediaplayer_volume then
        settings:AddConvarSetting(category_name, "float", mediaplayer_volume, "Media Player Volume", 0, 1, 2)
    end
    if pac_ogg_volume then
        settings:AddConvarSetting(category_name, "float", pac_ogg_volume, "PAC3 Volume", 0, 1, 2)
    end
end

local function graphics_settings()
    local settings = EasyChat.Settings
    local category_name = "Graphics"

    create_option_set(settings, category_name, {
        [cl_drawownshadow] = "Draw own shadow",
        [r_shadows] = "Draw shadows",
        [cl_playerspraydisable] = "Disable player sprays",
        [r_3dsky] = "3D Skybox",
        [r_WaterDrawReflection] = "Water Reflection",
        [r_WaterDrawRefraction] = "Water Refraction",
    })
end

hook.Add("ECPostLoadModules", "MiscSettings", function()
    audio_settings()
    graphics_settings()
end)

return "Misc Settings"