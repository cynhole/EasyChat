# EasyChat (Cyn Hole Fork)
"An open source advanced chat addon for users and developers. Modular, fully customizable and developer friendly EasyChat provides a good amount of features for both users and developers."

## Differences with this fork
This fork aims to replicate most of the stylings of Meta Construct's chatbox whilst still being EasyChat.

Such examples are:
* Meta styled settings
* No more color customization on chatbox UI
* ChatHUD offsets for PAC3 editor
* ATags support
* ChatHUD font customization
* ChatPrint color customization
* Mode selector menu (right click "Say" button)
* Better support with Themer

## Credits
* Cynthia - Maintainer
* homonovus - hnchat (settings menu ripped from it)
* Earu - Original Author
* The Cyn Hole - Ideas/Suggestions
* The Playground - Ideas/Suggestions, asking for ATags support

## Suggestions
Always open to suggestions. Feel free to make an issue.

## Contributing
"Any contributions is welcome, please do follow the naming conventions already in use though."